package com.chainofresponsibility.injection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class Chain<L extends Link> {
    protected List<L> links;

    public Chain(L... links){
        this.links = new ArrayList<L>(Arrays.asList(links));
    }

    public void addLink(L link){
        links.add(link);
    }


    protected static <L extends Link> L getLink(Object ob, List<L> links){
        L link = null;
        boolean attended= false;
        for (int i = 0; i < links.size(); i++) {
            link = links.get(i);

            if(attended =link.attend(ob)){

                if(links.size()>2) {
                    link = links.remove(i);
                    links.add(0, link);
                }
                break;
            }

        }
        if(!attended){
            link = null;
        }
        return link;
    }
}
