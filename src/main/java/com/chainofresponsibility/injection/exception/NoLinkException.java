package com.chainofresponsibility.injection.exception;

/**
 * Created by rramirezb on 19/01/2015.
 */
public class NoLinkException extends Exception{
    public NoLinkException(String message) {
        super(message);
    }
}
