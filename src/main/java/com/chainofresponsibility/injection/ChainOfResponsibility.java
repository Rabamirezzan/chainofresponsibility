package com.chainofresponsibility.injection;

import com.codeinjection.annotations.tool.DefaultClassInjectionParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by rramirezb on 13/01/2015.
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface ChainOfResponsibility {
    DefaultClassInjectionParams classParams() default @DefaultClassInjectionParams(classSuffix ="Chain");
    MethodReturn noLinkMethodReturn() default MethodReturn.RETURN_NULL;

}
