package com.chainofresponsibility.injection;

/**
 * Created by rramirezb on 13/01/2015.
 */
public interface Link {
    public boolean attend(Object ob);
}
