package com.chainofresponsibility.injection;

/**
 * Created by rramirezb on 19/01/2015.
 */
public enum MethodReturn {
    RETURN_NULL, THROW_EXCEPTION
}
